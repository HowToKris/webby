package com.ares.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ares.model.Dog;

@Controller
public class DefaultController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap model) {
		
		System.out.println("Hello");
		
		model.addAttribute("message", "Spring 4 MVC Hello World Example Built With Maven");
		return "hello";
	}
	
	@RequestMapping(value="/dogs", method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List jsonPage() {
		
		List<Dog> result = new ArrayList<Dog>();
		Dog dog1 = new Dog();
		dog1.setAge(3);
		dog1.setName("Sparky");
		dog1.setWeight(25);
		
		result.add(dog1);
		
		Dog dog2 = new Dog();
		dog2.setAge(1);
		dog2.setName("Sparkles");
		dog2.setWeight(20);
		result.add(dog2);
		
		return result;
	}
	
	
}
