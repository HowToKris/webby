package com.ares.controller;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import util.Mongodriver;

import com.ares.model.Card;
import com.ares.model.ResponseEntity;

@RestController
public class CardController {
	
	//query for cards
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/cards", method=RequestMethod.GET)
	@ResponseBody
	public ResponseEntity query() {
		
		Datastore ds = Mongodriver.getConnection();
		
		Query<Card> query = ds.createQuery(Card.class);
		List<Card> cards = query.asList();
		
		ResponseEntity response = new ResponseEntity(cards, 200, "Success");
		
		return response;
	}
	
	//create a card
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/cards", method=RequestMethod.POST)
	public ResponseEntity create(@RequestBody Card card) {
		
		Datastore ds = Mongodriver.getConnection();
		
		System.out.println("Received " + card.getName());
		card.setId(new ObjectId());
		
		// create a card with name Charmander but Charander exists. That's an error!
		// Therefore ResponseEntity response = (null, 500, "Error, a card named " + card.getName() + " already exists!")
		
		ds.save(card);
		
		ResponseEntity response;
		
		response = new ResponseEntity(null, 200, "Successfully Created");
		return response;
	}
	
		//delete a card
		@CrossOrigin(origins = "*")
		@RequestMapping(value="/cards", method=RequestMethod.DELETE)
		public ResponseEntity delete(@RequestBody Card card) {
			
			Datastore ds = Mongodriver.getConnection();
			
			System.out.println("Received " + card.getName());
			card.setId(new ObjectId());
			
			// mongodb.github.io/morphia/1.2/getting-started/quick-tour
			
			ds.save(card);
			
			ResponseEntity response;
			
			response = new ResponseEntity(null, 200, "Successfully Created");
			return response;
		}
}
