package test;

import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;

import com.ares.model.Card;

import util.Mongodriver;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Datastore ds = Mongodriver.getConnection();
		
			Card card = new Card();
			card.setName("Squirtle");
			card.setText("He squirts you with his love ;)");
			card.setType("Water");
			card.setAttack(2);
			card.setHealth(3);
			
			//Card card2 = new Card();
			//card2.setName("Charmander");
			//card2.setText("Char");
			//card2.setType("Fire");
			//card2.setAttack(3);
			//card2.setHealth(2);
			
			ds.save(card);
			System.out.println("Saved to db");
		}
}
