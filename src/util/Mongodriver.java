package util;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

public class Mongodriver {
	
	public static String DB_NAME = "webby";
	private static MongoClient mongoClient;
	
	private static final Morphia morphia = new Morphia();
	private static Datastore ds;
	
	
	public static Datastore getConnection() {
		if (ds != null) {
			return ds;
		}
		else {
			mongoClient = new MongoClient();
		
			mapPackages();
		
			ds = morphia.createDatastore(mongoClient, DB_NAME);		
			return ds;
		}	
	}
	
	private static void mapPackages() {
		
		morphia.mapPackage("com.ares.model");
		
	}
}
