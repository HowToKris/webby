package util;

import org.bson.UuidRepresentation;
import org.bson.codecs.UuidCodec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MongoDriver {
	
	public static String DB_NAME = "plopdb";
	private static MongoClient mongoClient;
	
	private static final Morphia morphia = new Morphia();
	private static Datastore ds;
	
	
	public static Datastore getConnection() {
		mongoClient = new MongoClient();
		
		mapPackages();
		
		ds = morphia.createDatastore(mongoClient, DB_NAME);		
		return ds;
	}
	
	private static void mapPackages() {
		
		morphia.mapPackage("com.zeenan.model");
		
	}

}
